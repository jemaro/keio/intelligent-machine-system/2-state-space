import numpy as np
import control
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

# Bigger figures
plt.rcParams["figure.figsize"] = map(
    lambda x: 1.5 * x, plt.rcParamsDefault["figure.figsize"]
    )

CONSTANTS = {
    # Process Constants
    'm': {
        'valmin': 0,
        'valmax': 10,
        'valinit': 1,
        'label': '$m$',
        },
    'M': {
        'valmin': 0,
        'valmax': 10,
        'valinit': 2,
        'label': '$M$',
        },
    'l': {
        'valmin': 0,
        'valmax': 10,
        'valinit': 2,
        'label': '$l$',
        },
    }


def system(m, M, l):
    g = 9.8
    A = np.array([
        [0, 0, 1, 0],
        [0, 0, 0, 1],
        [0, (-3 * m * g) / (m + 4 * M), 0, 0],
        [0, 3 * (M + m) * g / ((m + 4 * M) * l), 0, 0],
        ])
    B = np.array([
        [0],
        [0],
        [4 / (m + 4 * M)],
        [-3 / ((m + 4 * M) * l)],
        ])
    # C = np.eye(*A.shape)
    # C = np.array([[1, 0, 0, 0]])
    # C = np.array([[0, 0, 1, 0]])
    C = np.array([[0, 0, 0, 1]])
    # C = np.array([[0, 1, 0, 0]])
    D = np.zeros(shape=(C.shape[0], B.shape[1]))
    return control.StateSpace(A, B, C, D)


def update_step(ax, sys, step_range_slider):
    t, x = control.impulse_response(
        sys,
        T=np.linspace(0, step_range_slider.val, 1000),
        # input=0,
        # output=0,
        )
    ax.clear()
    ax.grid()
    ax.set_ylabel('Magnitude')
    ax.set_xlabel('Time [s]')
    ax.hlines(
        1, 0, 200, linestyles='dashed', colors='r', label=r'$\theta_{cmd}$'
        )
    ax.plot(t, x, label=r'$\theta_a$')
    ax.set_xlim(0, step_range_slider.val)
    ax.legend()
    ax.title.set_text('Step Response')
    return ax


def update_pzmap(ax, sys):
    ax.clear()
    ax.tick_params(labelbottom=False, labelleft=False)
    ax.axhline(y=0, color='black', lw=1)
    ax.axvline(x=0, color='black', lw=1)
    ax.axis('equal')
    if len(p := sys.pole()) > 0:
        ax.scatter(
            np.real(p),
            np.imag(p),
            s=50,
            marker='x',
            facecolors='k',
            label='poles'
            )
    if len(z := sys.zero()) > 0:
        ax.scatter(
            np.real(z),
            np.imag(z),
            s=50,
            marker='o',
            edgecolors='k',
            facecolors='none',
            label='zeros'
            )
    r = np.real(np.concatenate([z, p]))
    ax.set_xlim(np.min([1.1 * np.min(r), -1]), np.max([1.1 * np.max(r), 1]))
    ax.legend()
    ax.title.set_text('Pole/Zero map')
    return ax


if __name__ == '__main__':
    # Setup Plots
    fig, (ax_step, ax_poles) = plt.subplots(1, 2)
    plt.subplots_adjust(
        bottom=0.1 + 0.05 * len(CONSTANTS),
        left=0.075,
        right=0.95,
        wspace=0.05
        )

    # Interactive controls
    axcolor = 'lightgoldenrodyellow'
    sliders = dict()
    for i, (label, attributes) in enumerate(CONSTANTS.items()):
        sliders[label] = Slider(
            ax=plt.axes([0.3, 0.05 * (len(CONSTANTS) - i), 0.6, 0.03],
                        facecolor=axcolor),
            **attributes,
            )
    step_range_slider = Slider(
        ax=plt.axes([0.15, 0.95, 0.3, 0.03], facecolor=axcolor),
        valmin=10,
        valmax=200,
        valinit=100,
        label='xrange'
        )

    # Define update function
    def update(*args, **kwargs):
        sys = system(**{k: s.val for k, s in sliders.items()})
        # print(sys.output(0, [0, 0, 0, 0], 1))
        # Step Response Feedback System (Tracking System)
        update_step(ax_step, sys, step_range_slider)
        # # Poles and zeros
        update_pzmap(ax_poles, sys)

    # Interactive update
    for s in sliders.values():
        s.on_changed(update)
    step_range_slider.on_changed(update)

    # Initial update
    update()

    plt.show()