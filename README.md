# 2 State equation

Andreu Gimenez Bolinches ([andreu@keio.jp](mailto:andreu@keio.jp))

![assignment](./images/assignment.jpg)
\ 

## Assumptions

If we consider $x$ to be a distance unit $[m]$ and $\theta$ an angle $[rad]$
then an analysis of the state space equation reveals that $m$ and $M$ must be
masses $[kg]$ and $l$ must be a distance $[m]$ while $u$ must be a force
$[kg*m/s^2]=[N]$

$$ m > 0 $$
$$ M > 0 $$
$$ l > 0 $$

## Characteristic polynomial
In order to find the eigenvalues of $A$, we must find its characteristic
polynomial defined by $/sI-A/$ which results to:

$$s^4 - s^2\frac{3 g  (m+M)}{l (m+4 M)}$$

## Eigenvalues
Solving the roots of the characteristic polynomial we can find the eigenvalues
of $A$. Being the characteristic polynomial biquadratic, they are easy to find:

$$0$$
$$0$$
$$-\sqrt{\frac{3 g  (m+M)}{l (m+4 M)}}$$
$$\sqrt{\frac{3 g  (m+M)}{l (m+4 M)}}$$

This means that the system will have two poles of value 0 and two opposite
poles in the real axis, one positive and one negative. Which makes it unstable.

<!-- ## Controllability matrix -->